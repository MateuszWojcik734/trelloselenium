package utils;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.WebDriver;




public class WaitUtils {

    public static WebElement waitForElementToBeVisible(WebDriver driver, WebElement webElement, int seconds){

        WebDriverWait wait = new WebDriverWait(driver, seconds);

        WebElement element = wait.until(ExpectedConditions.visibilityOf(webElement));

        return element;

    }

    public static void waitForElementToBeNotVisible(WebDriver driver, WebElement webElement, int seconds){

        WebDriverWait wait = new WebDriverWait(driver, seconds);

        Boolean element = wait.until(ExpectedConditions.invisibilityOf(webElement));

    }
}
