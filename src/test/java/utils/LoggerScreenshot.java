package utils;

import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.apache.commons.io.FileUtils.copyFile;

public class LoggerScreenshot {

    private static Logger log = Logger.getLogger(LoggerScreenshot.class);

    private LoggerScreenshot() {
    }

    public static String TakeScreenshot(WebDriver driver, ITestResult TestResult) {
        final DateFormat DateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        final Date TestDate = new Date();
        final File Screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        final String screenshotPath = "Screenshots\\" + DateFormat.format(TestDate) + " "
                + TestResult.getName() + "\\" + TestResult.getName() + "-" + TestResult.getStatus() + ".jpg";
        try {
            copyFile(Screenshot, new File(screenshotPath));
        } catch (IOException ex) {
            log.error("There was a problem while taking a screenshot", ex);
        }
        return screenshotPath;
    }

}
