package test;

import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;
import PageFactory.LandingPage;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import PageFactory.HomePage;
import PageFactory.SignInPage;
import utils.LoggerScreenshot;


import static utils.LoggerScreenshot.TakeScreenshot;

public class TestSignIn {
    private static Logger log = Logger.getLogger(LoggerScreenshot.class);
    private WebDriver driver;




    @BeforeTest

    public void setup() {

        BasicConfigurator.configure();
        System.setProperty("webdriver.gecko.driver", "/Drivers/geckodriver.exe");
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://trello.com/");
    }


    @Test (testName = "Sign In - Positive")
    public void SignInPositive(){
        HomePage homePage = new HomePage(driver);
        homePage.checkHomePage();
        SignInPage signInPage = homePage.clickSignIn();
        signInPage.enterLogin("qweqweqweqwe@wp.com");
        signInPage.enterPassword("qweqweqwe");
        LandingPage landingPage = signInPage.clickLoginSukces();
        landingPage.checkPrompt();
        driver.quit();
    }

    @Test (testName = "Sign In - Fields Validation")
    public void SignInFieldsValidation(){

        HomePage homePage = new HomePage(driver);
        homePage.checkHomePage();
        SignInPage signInPage = homePage.clickSignIn();
        signInPage.clickLogin();
        signInPage.checkError();
        signInPage.enterLogin("testtest");
        signInPage.checkErrorDisappear();
        signInPage.clickLogin();
        signInPage.checkError();
        signInPage.enterLogin("qweqweqweqwe@wp.com");
        signInPage.checkErrorDisappear();
        signInPage.enterPassword("qweewq");
        signInPage.clickLogin();
        signInPage.checkError();
        signInPage.enterPassword("qweqweqwe1");
        signInPage.clickLogin();
        signInPage.checkError();
        driver.quit();
    }

    @AfterMethod

    protected void tearDown(ITestResult TestResult, Method method) {

        if (TestResult.getStatus() == ITestResult.FAILURE) {
            log.error("ERROR OCCURRED");
            final String screenshotPath = TakeScreenshot(driver, TestResult);
            log.info("SCREENSHOT HAS BEEN TAKEN" + " " + screenshotPath);
            log.info("FAIL TEST: " + method.getName());
        } else {
            log.info("SUCCESS TEST: " + method.getName());
        }
    }
}
