package test;

import PageFactory.HomePage;
import PageFactory.LandingPage;
import PageFactory.SignUpPage;
import PageFactory.SignUpPageEnterName;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import utils.LoggerScreenshot;
import java.lang.reflect.Method;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static utils.LoggerScreenshot.TakeScreenshot;

public class TestSignUp {


    private static Logger log = Logger.getLogger(LoggerScreenshot.class);
    private WebDriver driver;

    @BeforeTest

    public void setup() {

        BasicConfigurator.configure();
        System.setProperty("webdriver.gecko.driver", "/Drivers/geckodriver.exe");
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://trello.com/");
    }


    @Test (testName = "Sign Up - Positive")
    public void SignUpPositive(){

        HomePage homePage = new HomePage(driver);
        homePage.checkHomePage();
        SignUpPage signUpPage = homePage.clickSignUp();
        signUpPage.enterEmail(UUID.randomUUID().toString() + "@test.com");
        SignUpPageEnterName signUpPageEnterName = signUpPage.clickLoginSukces();
        signUpPageEnterName.enterName("testtesttest");
        signUpPageEnterName.enterPassword("test123!");
        LandingPage landingPage = signUpPageEnterName.clickLoginSukces();
        landingPage.checkPrompt();
        driver.quit();


    }

    @AfterMethod

    protected void tearDown(ITestResult TestResult, Method method) {

        if (TestResult.getStatus() == ITestResult.FAILURE) {
            log.error("ERROR OCCURRED");
            final String screenshotPath = TakeScreenshot(driver, TestResult);
            log.info("SCREENSHOT HAS BEEN TAKEN" + " " + screenshotPath);
            log.info("FAIL TEST: " + method.getName());
        } else {
            log.info("SUCCESS TEST: " + method.getName());
        }
    }


}
