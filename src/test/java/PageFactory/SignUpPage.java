package PageFactory;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.LoggerScreenshot;
import utils.WaitUtils;



public class SignUpPage {
    static WebDriver driver;
    private static Logger log = Logger.getLogger(LoggerScreenshot.class);

    @FindBy(id="email")
    private WebElement email;

    @FindBy(id="signup")
    private WebElement signupbtn;

    @FindBy(id="name")
    private WebElement name;


    public SignUpPage(WebDriver driver){

        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
    public SignUpPageEnterName clickLoginSukces(){
        log.info("clickLogin");
        WaitUtils.waitForElementToBeVisible(driver, signupbtn,10).click();
        return new SignUpPageEnterName(driver);
    }
    public void enterEmail(String randomEmail) {
        log.info("enterEmail");
        email.sendKeys(randomEmail);
    }

    public void enterName(String randomEmail) {
        log.info("enterName");
        WaitUtils.waitForElementToBeVisible(driver, signupbtn,10);
        email.sendKeys(randomEmail);
    }





}

