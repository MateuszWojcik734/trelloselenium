package PageFactory;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.LoggerScreenshot;
import utils.WaitUtils;

public class SignInPage {
    private static Logger log = Logger.getLogger(LoggerScreenshot.class);

    /**

     * All WebElements are identified by @FindBy annotation

     */

    WebDriver driver;

    @FindBy(id="user")
    private WebElement user;

    @FindBy(id="password")
    private WebElement password;

    @FindBy(id="login")
    private WebElement loginbtn;


    @FindBy (id="error")
    private WebElement error;


    public SignInPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);

    }



    public void clickLogin(){

        WaitUtils.waitForElementToBeVisible(driver, loginbtn,10).click();
    }

    public LandingPage clickLoginSukces(){
        log.info("clickLoginSukces");
        WaitUtils.waitForElementToBeVisible(driver, loginbtn,10).click();
        return new LandingPage(driver);
    }

    public void checkError(){
        log.info("checkError");
        WaitUtils.waitForElementToBeVisible(driver, error,10);
    }
    public void checkErrorDisappear(){
        log.info("checkErrorDisappear");
        WaitUtils.waitForElementToBeNotVisible(driver, error, 10);
    }

    public void enterLogin(String login){
        log.info("enterLogin");
        user.clear();
        user.sendKeys(login);
    }

    public void enterPassword(String Password){
        log.info("enterPassword");
        password.clear();
        password.sendKeys(Password);
    }



}