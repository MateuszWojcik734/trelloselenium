package PageFactory;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.LoggerScreenshot;
import utils.WaitUtils;

public class HomePage {

    static WebDriver driver;
    private static Logger log = Logger.getLogger(LoggerScreenshot.class);

    @FindBy(xpath = "//a[@href='/signup']")
    static WebElement SignUpButton;

    @FindBy(xpath = "//a[@href='/login']")
    static WebElement SignInButton;

    @FindBy(xpath = "//a[@href='/home']")
    private WebElement HomePageLogo;


    public HomePage(WebDriver driver){

        this.driver = driver;
        PageFactory.initElements(driver, this);

    }



    public static SignUpPage clickSignUp(){
        log.info("clickSignUp");
        WaitUtils.waitForElementToBeVisible(driver, SignUpButton,10).click();
        return new SignUpPage (driver);
    }
    public static SignInPage clickSignIn(){
        log.info("clickSignIn");
        WaitUtils.waitForElementToBeVisible(driver, SignInButton,10).click();
        return new SignInPage(driver);
    }
    public void checkHomePage(){
        log.info("checkHomePage");
        Assert.assertEquals(true, HomePageLogo.isDisplayed());
    }


}