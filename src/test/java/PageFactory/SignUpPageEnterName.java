package PageFactory;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.LoggerScreenshot;
import utils.WaitUtils;

public class SignUpPageEnterName {

    static WebDriver driver;
    private static Logger log = Logger.getLogger(LoggerScreenshot.class);

    @FindBy(id="email")
    private WebElement email;

    @FindBy(id="signup")
    private WebElement signupbtn;

    @FindBy(id="name")
    private WebElement name;

    @FindBy(id="password")
    private WebElement password;

    public SignUpPageEnterName(WebDriver driver){

        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
    public LandingPage clickLoginSukces(){
        log.info("clickLogin");
        WaitUtils.waitForElementToBeVisible(driver, signupbtn,10).click();
        return new LandingPage(driver);
    }
    public void enterPassword(String UserPassword) {
        log.info("enterPassword");
        password.sendKeys(UserPassword);
    }

    public void enterName(String UserName) {
        log.info("enterName");
        WaitUtils.waitForElementToBeVisible(driver, name,10);
        name.sendKeys(UserName);
    }





}
