package PageFactory;


import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import utils.LoggerScreenshot;
import utils.WaitUtils;


public class LandingPage {

    WebDriver driver;
    private static Logger log = Logger.getLogger(LoggerScreenshot.class);

    @FindBy(xpath = "//div[@class='board-name']")
    private WebElement FirstMessage;

    public LandingPage(WebDriver driver){

        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
    public void checkPrompt(){
        WaitUtils.waitForElementToBeVisible(driver, FirstMessage,10);
    }

}


